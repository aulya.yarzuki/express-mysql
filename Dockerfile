# Use official Node.js image as base
FROM node:14

# Set working directory inside the container
WORKDIR /src

# Copy package.json and package-lock.json files
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the port your app runs on
EXPOSE 4000

# Command to run your application
CMD ["node", "src", "index.js"]
