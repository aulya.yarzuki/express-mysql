const express = require('express');

const router = express.Router();

const userController = require('../controllers/users');

// const authController = require('../controllers/authController');

const verifyToken = require('../middleware/verifyToken');

// CREATE POST REQUEST
router.post('/', userController.CreateNewUser);

// READ GET REQUEST
router.get('/',verifyToken ,userController.getAllUsers);

// UPDATE PATCH REQUEST
router.patch('/:id', userController.UpdateUser);

// DELETE REQUEST
router.delete('/:id', userController.deleteUser);

// router.post('/register', authController.registerUser);
// app.post('/register', authController.registerUser);


// router.post('/', (req, res, next) => {
//   res.json({
//     message: 'POST user success'
//   });
// });

console.log(router)

module.exports = router;