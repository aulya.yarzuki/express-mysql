require('dotenv').config()
const APP_PORT = process.env.PORT;

const express = require('express');
const app = express();

const usersRoutes = require('./routes/users.js');
const authController = require('./controllers/authController');
const middlewareLogRequest = require('./middleware/logs.js');
const dbUsers = require('./models/users.js');
const tryAxios = require('./controllers/axiosController.js');
const verifyToken = require('./middleware/verifyToken');

// MIDDLEWARE
app.use(express.json()); // Use body-parser to handle request JSON
app.use(middlewareLogRequest);
app.use('/assets', express.static('public'));

// Define a route handler for POST /register
app.post('/register', authController.registerUser);

// Define a route handle for POST /login
app.post('/login', authController.loginUser);
// app.post('/login', (req, res) => {
//     // Logic to handle the /login endpoint
//     authController.loginUser(req, res);
// });

// Define a route handle for try GET /Axios
app.get('/axios', verifyToken, tryAxios.fetchApi);

app.use('/users', usersRoutes);

// Error handling middleware
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

app.listen(APP_PORT, () => {
    console.log(`Server is running on port ${APP_PORT}`);
});