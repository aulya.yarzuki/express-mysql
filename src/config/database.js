const mysql = require('mysql2');

// Create the connection pool. The pool-specific settings are the defaults
const env = process.env
const dbPool = mysql.createPool({
  host: env.DB_HOST,
  user: env.DB_USER,
  database: env.DB_NAME,
  password: env.DB_PASSWORD,
  waitForConnections: true,
  enableKeepAlive: true,
  keepAliveInitialDelay: 0,
});

module.exports = dbPool.promise();