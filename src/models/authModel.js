const bcrypt = require('bcrypt');
const saltRounds = 10;

const registerUser = (body) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(saltRounds, function(err, salt) {
            if (err) return reject(err);
            bcrypt.hash(body.password, salt, function(err, hash) {
                if (err) return reject(err);
                let SQLQuery = `INSERT INTO Auth (username, email, password) VALUES (?, ?, ?)`;
                const values = [body.username, body.email, hash];
                dbPool.execute(SQLQuery, values)
                    .then(result => resolve(result))
                    .catch(err => reject(err));
            });
        });
    });
}

const loginUser = (body) => {
    return new Promise((resolve, reject) => {
        let SQLQuery = `SELECT * FROM Auth WHERE username='${body.username}'`;
        dbPool.execute(SQLQuery)
            .then(result => resolve(result))
            .catch(err => reject(err));
    });
}

module.exports = { registerUser,
    loginUser  };