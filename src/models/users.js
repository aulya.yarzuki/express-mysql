dbPool = require('../config/database');

const getAllUsers = () => {
    const SQLQuery = 'SELECT * FROM `Users`';

    // dbPool.execute(
    //     SQLQuery,
    //     ['Rick C-137', 53],
    //     (err, results, fields) => {
    //     //   console.log(results); // results contains rows returned by server
    //       console.log(fields); // fields contains extra meta data about results, if available
    //       if(err){
    //         res.status(500).json({
    //             message: 'Internal Server Error',
    //             error: err
    //         });
    //       }
    //       res.json({
    //           message: 'GET user success',
    //           data: results
    //       });
    //     }
    // );
    return dbPool.execute(SQLQuery);
}

const CreateNewUser = (body) => {
    // console.log(req.body);
    let SQLQuery = `INSERT INTO Users (name, email, address) VALUES ('${body.name}', '${body.email}', '${body.address}')`;
    return dbPool.execute(SQLQuery)
}

const UpdateUser = (body, id) => {
    const SQLQuery = `UPDATE Users 
                      SET name=?, email=?, address=? 
                      WHERE id=?`;
    const values = [body.name, body.email, body.address, id];

    return dbPool.execute(SQLQuery, values);
}

const deleteUser = (id) => {
    const SQLQuery = `DELETE FROM Users 
                      WHERE id=?`;
    const values = [id];

    return dbPool.execute(SQLQuery, values);
}


module.exports = {getAllUsers,
    CreateNewUser,
    UpdateUser,
    deleteUser
};