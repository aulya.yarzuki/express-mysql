const authModel = require('../models/authModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config()

const saltRounds = 10;

const loginUser = async(req, res) => {
    const {body} = req;
    try {
        const [data] = await authModel.loginUser(body);
        
        if (data.length === 0) {
            res.status(404).json({
                message: 'User not found',
                data: data
            });
        } else {
            const compare = bcrypt.compareSync(body.password, data[0].password);
            if (compare) {
                const token = jwt.sign({_username: data[0].username}, process.env.TOKENJ);
                res.header('auth-token', token);
                res.status(200).json({
                    message: 'Login success',
                    data: data,
                    // 'auth-token': token
                });
            } else {
                res.status(401).json({
                    message: 'Uname/Password is incorrect',
                    data: data
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            message: 'Internal Server Error',
            error: error
        });
    }
}

const registerUser = async(req, res) => {
    const {body} = req;
    // console.log(body);
    const hash = bcrypt.hashSync(body.password, saltRounds);
    
    try {
        await authModel.registerUser(body);
        res.status(201).json({
            message: 'CREATE new user success',
            data: {
                username: body.username,
                email: body.email,
                password: hash
            }
        })
    } catch (error) {
        res.status(500).json({
            message: 'Server Error',
            serverMessage: error,
        })
    }
}

module.exports = {registerUser,
    loginUser};