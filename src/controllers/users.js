const UsersModel = require('../models/users');
const getAllUsers = async(req, res) => {

  try {
    const [data] = await UsersModel.getAllUsers();

    res.json({
        message: 'GET user success',
        data: data
    });
  } catch (error) {
    res.status(500).json({
        message: 'Internal Server Error',
        error: error
    });
  }
  
}

const CreateNewUser = async(req, res) => {
    // const {body} = req;
    const payloadReq = req.body;
    // console.log(payloadReq);
    try {
        // await UsersModel.CreateNewUser(body);
        await UsersModel.CreateNewUser(payloadReq);
        res.status(201).json({
            message: 'CREATE new user success',
            data: payloadReq
        })
    } catch (error) {
        res.status(500).json({
            message: 'Server Error',
            serverMessage: error,
        })
    }
}

const UpdateUser = async(req, res) => {
    const {id} = req.params;
    const {body} = req;

    try {
        await UsersModel.UpdateUser(body, id);
        res.json({
            message: 'UPDATE User Success',
            data: body
        });
    } catch (error) {
        res.status(500).json({
            message: 'Internal Server Error',
            error: error
        });
    }
}

const deleteUser = async(req, res) => {
    const {id} = req.params;
    // const {body} = req;

    try {
        await UsersModel.deleteUser(id);
        res.json({
            message: 'DELETE User Success',
            data: null
            // data: req.body
        });
    } catch (error) {
        res.status(500).json({
            message: 'Internal Server Error',
            error: error
        });
    }
    
}

module.exports = {
    getAllUsers,
    CreateNewUser,
    UpdateUser,
    deleteUser
}