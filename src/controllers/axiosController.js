const axios = require('axios');
// Membuat permintaan GET ke URL tertentu
// const fetchApi = () => {
//   return axios.get('https://api.openweathermap.org/data/2.5/weather?lat=44.34&lon=10.99&appid=9ec9f91b16dd9542e6a135680de5265b')
//     .then(response => {
//       // Mengakses data dari respons
//       console.log(response.data);
//       // response.data;
//     })
//     .catch(error => {
//       // Menangani kesalahan
//       console.error('Error fetching data:', error);
//     });
// }

// Function to fetch data from the API
const fetchApi = async (req, res) => {
  try {
    // Make API request
    const response = await axios.get('https://api.openweathermap.org/data/2.5/weather?lat=44.34&lon=10.99&appid=9ec9f91b16dd9542e6a135680de5265b');
    // Extract and return data from the response
    // return response.data;
    res.status(200).json(response.data);
  } catch (error) {
    // Throw error if request fails
    throw new Error('Failed to fetch data from API');
  }
};

module.exports = { fetchApi };