const pdfMake = require('pdfmake/build/pdfmake.js');
const pdfFonts = require('pdfmake/build/vfs_fonts.js');
const express = require('express')
pdfMake.vfs = pdfFonts.pdfMake.vfs;

// const fs = require('fs');
const path = require('path');

const axios = require('axios');

const fetchData = async (req) => { // Accept the req object as a parameter
  try {
    authToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfdXNlcm5hbWUiOiJhZG1pbjciLCJpYXQiOjE3MDg1NzU2NzZ9.03TviU7OTLpeW-_TypWjDvQEu1edkIMgOAFJtXjuxHc'
    // Make a GET request to the /fetch-data endpoint with the authorization token included in the headers
    const response = await axios.get('http://localhost:4000/axios', {
      headers: {
        'auth-token': authToken
      }
    }); // Replace with your server URL
    
    // Log the response data to the console
    console.log(response.data);
    // You can further process the response data here (e.g., generate PDF)
  } catch (error) {
    // Handle errors
    console.error('Error fetching data:', error);
  }
};

// module.exports = fetchData; // Export the fetchData function
// Call the fetchData function
const weatherData = fetchData();

// // Define font files
const fonts = {
    Roboto: {
      normal: path.join(__dirname, "/fonts/Roboto-Regular.ttf"),
      bold: path.join(__dirname, "/fonts/Roboto-Medium.ttf"),
      italics: path.join(__dirname, "/fonts/Roboto-Italic.ttf"),
      bolditalics: path.join(__dirname, "/fonts/Roboto-MediumItalic.ttf")
    }
};
  
let PdfPrinter = require('pdfmake');
let printer = new PdfPrinter(fonts);
let fs = require('fs');
  
let docDefinition = {
    // ...
    content: [
		{ text: 'Weather Simulation', style: 'header', fontSize: 25, alignment: "center" },
		'Official documentation is in progress, this document is just a glimpse of what is possible with pdfmake and its layout engine.',
        {
			      image: './public/logo.jpg',
			      fit: [450, 450],
            alignment: 'center'
        },
        {
            text: 'Weather Data',
            style: 'subheader',
            fontSize: 18,
            bold: true,
            margin: [0, 20, 0, 8]
        },
        {
            text: 'The weather data is as follows:',
            margin: [0, 0, 0, 20]
        },
        {
            ul: [
                'Temperature: 25°C',
                'Humidity: 50%',
                'Wind Speed: 10 km/h',
                'Pressure: 1013 hPa'
            ]
        }
    ]
};  
  
// let options = {
    // ...
// }
  
// let pdfDoc = printer.createPdfKitDocument(docDefinition, options);
let pdfDoc = printer.createPdfKitDocument(docDefinition);
pdfDoc.pipe(fs.createWriteStream('./public/pdf/document2.pdf'));
pdfDoc.end();